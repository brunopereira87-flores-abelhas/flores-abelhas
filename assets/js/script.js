const base_url = 'http://localhost/flores_abelhas/';
const btnAbelha = document.getElementById('btn-abelha');
const btnCancelar = document.querySelector('.btn-cancelar');
const btnFechar = document.querySelector('.btn-close');
const mobileButton = document.querySelector('.mobileButton');
const abelhasElement = document.querySelector('.abelhas');
const hiddenInput = document.querySelector('input[name=abelhas_id]');
const inputImagem = document.querySelector('#imagem');
let abelhasSelecionadas = [];
let mesesSelecionados = [];
let abelhasDom = [];

const abelhaSelect = document.querySelector('.abelha_select');
const mesesCheck = document.querySelectorAll('.checkbutton input');

if (btnCancelar) {
    btnCancelar.addEventListener('click', (e) => window.location.href = base_url);
}
if (btnFechar) {
    btnFechar.addEventListener('click', (e) => {
        document.querySelector('.menu_mobile').classList.remove('show');
    });
}
if (mobileButton) {
    mobileButton.addEventListener('click', (e) => {
        document.querySelector('.menu_mobile').classList.add('show');
    });
}
if (flores !== undefined) {
    refreshFloresDom();
}
if (abelhaSelect) {
    abelhaSelect.addEventListener('change', function (ev) {
        const abelhaValue = ev.target.value;
        const abelhaObj = abelhas.find(item => abelhaValue === item.id);

        addAbelha(abelhaObj);
        if (abelhaSelect.hasAttribute('busca')) buscaFlores();
    });
}


mesesCheck.forEach(mes => {
    mes.addEventListener('change', (e) => {
        console.log('change')
        if (mesesSelecionados.includes(e.target.value)) {
            removeMes(e.target.value);
            mes.parentNode.querySelector('label').classList.remove('checked');
        }
        else {
            mesesSelecionados.push(e.target.value);
            mes.parentNode.querySelector('label').classList.add('checked');
        }
        if (mes.hasAttribute('busca')) {
            buscaFlores();
        }
    })
})
function removeMes(mes) {
    const pos = mesesSelecionados.indexOf(mes)
    mesesSelecionados.splice(pos, 1);
}
async function buscaFlores() {
    let url = `${base_url}/home/flores`
    const abelhas_query = abelhasSelecionadas.map(abelha => abelha.id).join(',');
    const meses_query = mesesSelecionados.map(mes => mes).join(',');

    const formdata = new FormData();
    formdata.append('abelhas', abelhas_query);
    formdata.append('meses', meses_query);

    try {
        const response = await fetch(url, {
            method: 'POST',
            body: formdata
        });

        flores = await response.json();
        refreshFloresDom();
    } catch (err) {
        console.log(err.message)
    }

}

function refreshFloresDom() {
    const flores_dom = flores.reduce((acc, flor) => {
        const dom =
            `<div class="flor" flor_id="${flor['id']}">
            <img src="${base_url}assets/img/flores/${flor['imagem']}">
            <span>${flor['nome']}</span>
        </div> `;

        return acc + dom;
    }, '');

    document.querySelector('.flores').innerHTML = flores_dom;
    addModalEvent(document.querySelectorAll('.flor'))
}

function addModalEvent(elements) {
    elements.forEach(elem => {
        elem.addEventListener('click', (e) => {
            const flor = e.currentTarget;
            openModal(flor.getAttribute('flor_id'));

        })
    });
}

async function openModal(flor_id) {
    const flor = flores.find(flor_item => flor_item.id === flor_id);
    const abelhas_flor = await getFlorAbelhas(flor_id);
    console.log('flor:', flor)
    console.log('abelhas_flor:', abelhas_flor)
    setModalContent(flor, abelhas_flor);
    const modal = document.querySelector('.modal');
    modal.classList.add('show');
    modal.addEventListener('click', (e) => {
        if (e.target === e.currentTarget) {
            modal.classList.remove('show');
        }
    })

}

function setModalContent(flor, abelhas_flor) {
    const modal_content = document.querySelector('.flor_detalhes');

    modal_content.querySelector('#imagem_flor').setAttribute('src',
        `${base_url}assets/img/flores/${flor.imagem}`);
    modal_content.querySelector('#nome_flor').innerText = flor.nome;
    modal_content.querySelector('#descricao_flor').innerText = flor.descricao || 'Sem descrição';

    const abelhas_flor_dom = modal_content.querySelector('#abelhas_flor');
    if (abelhas_flor && abelhas_flor.length > 0) {
        const abelha_lista = document.createElement('ul');
        abelha_lista.classList.add('abelhas_lista');
        abelhas_flor.forEach(abelha => {
            const abelha_li = document.createElement('li');
            abelha_li.innerText = abelha.nome;
            abelha_lista.append(abelha_li);
        });

        console.log('abelista:', abelhas_flor_dom)
        abelhas_flor_dom.innerHTML = abelha_lista.outerHTML;
    } else {
        const sem_abelhas = document.createElement('p');
        sem_abelhas.classList.add('sem_abelhas');
        sem_abelhas.innerText = 'Nenhuma abelha poliniza esta flor';
        abelhas_flor_dom.innerHTML = sem_abelhas.outerHTML;
    }


}
async function getFlorAbelhas(flor_id) {
    let flor = null;
    try {
        const response = await fetch(`${base_url}home/flor_abelhas/${flor_id}`);
        flor = response.json();

    } catch (err) {
        console.log(err.message)
    }
    finally {
        return flor;
    }
}
if (btnAbelha) {
    console.log('tem btn abelha')
    btnAbelha.addEventListener('click', function (e) {
        e.preventDefault();
        abelhaSelect.classList.add('show');
    })
}

if (inputImagem) {
    inputImagem.addEventListener('change', previewImagem);
}
function addAbelha(item) {
    if (item && item.nome) {
        let abelha = {
            text: item.nome,
            element: document.createElement('span'),
        };

        abelha.element.classList.add('abelha');
        abelha.element.setAttribute('abelha_id', item.id)
        abelha.element.textContent = abelha.text;

        let closeBtn = document.createElement('span');
        closeBtn.setAttribute('abelha_id', item.id)
        closeBtn.classList.add('close');
        closeBtn.addEventListener('click', function () {
            removeAbelha(abelhasDom.indexOf(abelha));
        });
        abelha.element.appendChild(closeBtn);

        abelhasSelecionadas.push(item);
        abelhasDom.push(abelha);
        abelhasElement.appendChild(abelha.element);

        refreshAbelhas();
    }

}

function previewImagem(e) {
    const preview = document.querySelector('#img-input');

    const img = e.target.files[0];
    preview.setAttribute('src', URL.createObjectURL(img));
}
function refreshAbelhas() {
    let abelhasList = [];
    abelhasSelecionadas.forEach(function (abelha) {
        abelhasList.push(abelha.id);
    });
    hiddenInput.value = abelhasList.join(',');
}
// [].forEach.call(document.getElementsByClassName('abelha_input'), function (el) {
//     let hiddenInput = document.createElement('input'),
//         mainInput = document.createElement('input'),
//         abelhas = [];

//     hiddenInput.setAttribute('type', 'hidden');
//     hiddenInput.setAttribute('name', el.getAttribute('data-name'));

//     mainInput.setAttribute('type', 'text');
//     mainInput.classList.add('main-input');
//     mainInput.addEventListener('input', function () {
//         let enteredAbelhas = mainInput.value.split(',');
//         if (enteredAbelhas.length > 1) {
//             enteredAbelhas.forEach(function (t) {
//                 let filteredAbelha = filterAbelha(t);
//                 if (filteredAbelha.length > 0)
//                     addAbelha(filteredAbelha);
//             });
//             mainInput.value = '';
//         }
//     });

//     mainInput.addEventListener('keyup', function (e) {

//     })
//     mainInput.addEventListener('keydown', function (e) {
//         let keyCode = e.which || e.keyCode;
//         if (keyCode === 8 && mainInput.value.length === 0 && abelhas.length > 0) {
//             removeAbelha(abelhas.length - 1);
//         }
//     });

//     el.appendChild(mainInput);
//     el.appendChild(hiddenInput);

//     addAbelha('hello!');

//     function addAbelha(text) {
//         let abelha = {
//             text: text,
//             element: document.createElement('span'),
//         };

//         abelha.element.classList.add('abelha');
//         abelha.element.textContent = abelha.text;

//         let closeBtn = document.createElement('span');
//         closeBtn.classList.add('close');
//         closeBtn.addEventListener('click', function () {
//             removeAbelha(abelhas.indexOf(abelha));
//         });
//         abelha.element.appendChild(closeBtn);

//         abelhas.push(abelha);

//         el.insertBefore(abelha.element, mainInput);

//         refreshAbelhas();
//     }

//     function removeAbelha(index) {
//         let abelha = abelhas[index];
//         abelhas.splice(index, 1);
//         el.removeChild(abelha.element);
//         refreshAbelhas();
//     }

//     function refreshAbelhas() {
//         let abelhasList = [];
//         abelhas.forEach(function (t) {
//             abelhasList.push(t.text);
//         });
//         hiddenInput.value = abelhasList.join(',');
//     }

//     function filterAbelha(abelha) {
//         return abelha.replace(/[^\w -]/g, '').trim().replace(/\W+/g, '-');
//     }
// });
