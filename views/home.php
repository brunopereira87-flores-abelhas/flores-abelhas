
<div class="conteudo">   
  <section class="calendario_flores">
    <script>
      var abelhas = <?php echo json_encode($abelhas)?>
    </script>
    <script>
      var meses = <?php echo json_encode($meses)?>
    </script>
    <script>
      var flores = <?php echo json_encode($flores)?>
    </script>
    <section class="top">
      <h1 class="page_title">Calendário de Flores</h1>
      <div class="link_buttons">
          <a href="<?=BASE_URL?>cadastrar/flor" class="btn btn-primary">Cadastrar Flor</a>
          <a href="<?=BASE_URL?>cadastrar/abelha" class="btn btn-primary">Cadastrar Abelha</a>
      </div>
      <div class="menu_mobile">
        <button type="button" class="btn-close">X</button>
        <div class="link_buttons">
            <a href="<?=BASE_URL?>cadastrar/flor" class="btn btn-primary">Cadastrar Flor</a>
            <a href="<?=BASE_URL?>cadastrar/abelha" class="btn btn-primary">Cadastrar Abelha</a>
        </div>
      </div>
      <button class="mobileButton" aria-label="Menu"></button>
    </section>
    
    <section class="calendar">
      <p class="description">Neste calendário encontram-se diversas flores. </p>
      <p class="description">Podem ser agrupadas pelos meses que  florescem e/ou pelo 
        tipo de abelha que poliniza a flor.
      </p>

      <div class="form-group abelha_input">
        <label for="abelha">Selecione as abelhas</label>
        <input type="hidden" name="abelhas_id" />
        <div class="abelha_groups">
          
          <select class="abelha_select form-control" busca name="abelha" id="abelha">
            <option value="">...Selecione uma abelha</option>
            <?php 
              print_r($abelhas);
              foreach($abelhas as $abelha ):
            ?>
            <option value="<?=$abelha['id']?>"><?=$abelha['nome']?>(<?=$abelha['especie']?>)</option>
            <?php
              endforeach;
            ?>
          </select>
          <div class="abelhas">
          </div>
        </div>
        <div class="month_buttons">
          <?php 
            foreach($meses as $mes ):
          ?>
          <div class="checkbutton">
            <input type="checkbox" busca name="meses[]" value="<?=$mes['id']?>" id="<?=$mes['abreviacao']?>">
            <label for="<?=$mes['abreviacao']?>"><?=$mes['abreviacao']?></label>
          </div>
          <?php
            endforeach;
          ?>
          </div>
      </div>
      <div class="flores">
        <!-- <?php
          if(isset($flores) && count($flores) > 0):
            foreach($flores as $flor) :
        ?>
        <div class="flor">
          <img src="<?=BASE_URL?>assets/img/flores/<?=$flor['imagem']?>">
          <span><?=$flor['nome']?></span>
        </div>
        <?php
            endforeach;
          endif;
        ?> -->
      </div>
      <div class="modal">
        <div class="flor_detalhes">
          <img id="imagem_flor" />
          <div class="dados_flor">
            <h4 id="nome_flor"></h4>
            <p id="descricao_flor"></p>
            <h4>Abelhas</h4>
            <div id="abelhas_flor">
            </div>
          </div>
          
        </div>
      </div>
    </section>
  </section>
</div>