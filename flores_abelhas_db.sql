-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 03-Dez-2020 às 15:18
-- Versão do servidor: 5.7.23
-- versão do PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flores_abelhas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `abelhas`
--

DROP TABLE IF EXISTS `abelhas`;
CREATE TABLE IF NOT EXISTS `abelhas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `especie` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `abelhas`
--

INSERT INTO `abelhas` (`id`, `nome`, `especie`) VALUES
(1, 'Uruçu', 'Melipona scutellaris'),
(2, 'Uruçu-Amarela', 'Melipona rufiventris'),
(4, 'Guarupu', 'Melipona bicolor'),
(9, 'Iraí', 'Nannotrigona testaceicornes'),
(10, 'Jataí', 'Tetragonisca angustula'),
(11, 'Jataí-da-Terra', 'Paratrigona subnuda'),
(12, 'Mandaçaia', 'Melipona mandaçaia'),
(13, 'Manduri', 'Melipona marginata'),
(14, 'Tubuna', 'Scaptotrigona bipunctata'),
(15, 'Mirim Droryana', 'Plebeia droryana'),
(16, 'Mirim-Guaçu', 'Plebeia remota'),
(17, 'Mirim-Preguiça', 'Friesella Schrottkyi'),
(18, 'Lambe-Olhos', 'Leurotrigona muelleri'),
(19, 'Borá', 'Tetragona clavipes'),
(20, 'Boca-de-Sapo', 'Partamona helleri'),
(21, 'Guira', 'Geotrigona mombuca'),
(22, 'Marmelada Amarela', 'Frieseomelitta varia'),
(23, 'Mombucão', 'Cephalotrigona capitata'),
(24, 'Guiruçu', 'Schwarziana quadripunctata'),
(25, 'Tataíra', 'Oxytrigona tataira tataira'),
(26, 'Irapuã', 'Trigona spinipes'),
(27, 'Abelha-Limão', 'Lestrimelitta limao'),
(28, 'Bieira', 'Mourella caerulea');

-- --------------------------------------------------------

--
-- Estrutura da tabela `flores`
--

DROP TABLE IF EXISTS `flores`;
CREATE TABLE IF NOT EXISTS `flores` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `especie` varchar(255) NOT NULL,
  `descricao` text,
  `imagem` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `flores`
--

INSERT INTO `flores` (`id`, `nome`, `especie`, `descricao`, `imagem`) VALUES
(1, 'Laranjeira', 'Citrus X sinensis', '', '67b040d7f431ee4ab3ef5b64afb749bb.jpg'),
(2, 'Limoeiro', 'Citrus x limon', '', 'e9009106d065a316a3373a7cb645b205.jpg'),
(3, 'Orquídea', 'Orchidaceae', '', 'c2d66949ef71244d2a63ec98e9dee778.jpg'),
(4, 'Goiabeira', 'Psidium guajava', '', 'flower_icon.png'),
(5, 'Ipê', 'Tabebuia', '', 'ff2a711db5a64b9b29ecfb4ad599f1dd.jpg'),
(6, 'Rosa', 'Rosoidae', 'É uma rosa', 'flower_icon.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `flores_abelhas`
--

DROP TABLE IF EXISTS `flores_abelhas`;
CREATE TABLE IF NOT EXISTS `flores_abelhas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `abelha_id` int(10) UNSIGNED NOT NULL,
  `flor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `flores_abelhas`
--

INSERT INTO `flores_abelhas` (`id`, `abelha_id`, `flor_id`) VALUES
(1, 2, 1),
(2, 1, 1),
(3, 2, 2),
(4, 10, 2),
(5, 10, 2),
(6, 10, 3),
(7, 0, 4),
(8, 9, 5),
(37, 10, 6),
(36, 1, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `flores_meses`
--

DROP TABLE IF EXISTS `flores_meses`;
CREATE TABLE IF NOT EXISTS `flores_meses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flor_id` int(10) UNSIGNED NOT NULL,
  `mes_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `flores_meses`
--

INSERT INTO `flores_meses` (`id`, `flor_id`, `mes_id`) VALUES
(1, 1, 1),
(2, 1, 4),
(3, 2, 3),
(4, 2, 10),
(5, 3, 4),
(6, 3, 11),
(7, 4, 5),
(8, 4, 10),
(9, 5, 3),
(37, 6, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `meses`
--

DROP TABLE IF EXISTS `meses`;
CREATE TABLE IF NOT EXISTS `meses` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `numero` tinyint(4) NOT NULL,
  `nome` varchar(32) NOT NULL,
  `abreviacao` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `meses`
--

INSERT INTO `meses` (`id`, `numero`, `nome`, `abreviacao`) VALUES
(1, 1, 'Janeiro', 'Jan'),
(2, 2, 'Fevereiro', 'Fev'),
(3, 3, 'Março', 'Mar'),
(4, 4, 'Abril', 'Abril'),
(5, 5, 'Maio', 'Maio'),
(6, 6, 'Junho', 'Jun'),
(7, 7, 'Julho', 'Jul'),
(8, 8, 'Agosto', 'Ago'),
(9, 9, 'Setembro', 'Set'),
(10, 10, 'Outubro', 'Out'),
(11, 11, 'Novembro', 'Nov'),
(12, 12, 'Dezembro', 'Dez');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
