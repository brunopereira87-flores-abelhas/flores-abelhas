<?php

class homeController extends Controller{
    private $abelha;
    private $flor;
    private $mes;

    public function __construct(){
        $this->abelha = new Abelha();
        $this->flor = new Flor();
        $this->mes = new Mes();
    }
    public function index(){
        $dados = array(
            'title'=>'Calendário de Flores',
            'main_title'=>'Calendário de Flores',
            'css'=>'home',
            'header_type' => 'list'
        );        
        
        $dados['abelhas'] = $this->abelha->getAbelhas();
        $dados['meses'] = $this->mes->getMeses();
        $dados['flores'] = $this->flor->getFlores();
        $this->loadTemplate("home",$dados);
    }
    public function flores(){
        $meses = filter_input(INPUT_POST, 'meses', FILTER_SANITIZE_STRING);
        $abelhas = filter_input(INPUT_POST, 'abelhas', FILTER_SANITIZE_STRING);

        if(isset($meses) && !empty($meses) && isset($abelhas) && !empty($abelhas)){
            $query = "SELECT DISTINCT f.* FROM flores f INNER JOIN flores_meses fm ON f.id = fm.flor_id ". 
            "INNER JOIN flores_abelhas fa ON f.id = fa.flor_id ". 
            "WHERE f.id = fm.flor_id AND fm.mes_id IN ($meses) OR f.id = fa.flor_id ".
            "AND fa.abelha_id IN ($abelhas)";
        }else if(isset($meses) && !empty($meses)){
            $query = "SELECT DISTINCT f.* FROM flores f INNER JOIN flores_meses fm ON f.id = fm.flor_id ". 
            "WHERE f.id = fm.flor_id AND fm.mes_id IN ($meses) ";
        }else if(isset($abelhas) && !empty($abelhas)){
            $query = "SELECT DISTINCT f.* FROM flores f INNER JOIN flores_abelhas fa ON f.id = fa.flor_id ". 
            "WHERE f.id = fa.flor_id AND fa.abelha_id IN ($abelhas) ";
        }
        else {
            $query = "SELECT * FROM flores";
        }
        
        $flores = $this->flor->query($query);
        
        echo json_encode($flores);
    }

    public function flor_abelhas($flor_id){
        $sql = "SELECT DISTINCT a.nome FROM abelhas a INNER JOIN flores_abelhas fa ON a.id = fa.abelha_id ".
        "WHERE fa.flor_id = $flor_id";

        $query_abelhas = $this->flor->query($sql);
        echo json_encode($query_abelhas);
    }
    // private function getCursos($usuario,$id_usuario){
    //     $curso = new Curso();
        
    //     if($usuario->getNivel($id_usuario) == 1){          
    //         $usuario = new Aluno();
    //         $cursos = $curso->getCursosDoAluno($id_usuario);
    //         if(count($cursos)<=0){
    //             $_SESSION['semcurso'] = 'Você ainda não está matriculado em nenhum curso. '
    //                     . '<a href="'.BASE_URL.'/cursos'.'">Clique e conheça os nossos cursos!</a>';
    //         }
    //     }
    //     else{
    //         $cursos = $curso->getCursosDoInstrutor($id_usuario);
    //         if(count($cursos)<=0){
    //             $_SESSION['semcurso'] = 'Você ainda não criou nenhum curso';
    //         }
    //     }
        
    //     return $cursos;
    // }
}