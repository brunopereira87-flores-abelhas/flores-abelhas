<?php

class Flor extends Model{
  public function where($campos_array, $campos_condicao, $valores, $tabela = "flores") {
    $resultado = parent::where($campos_array, $campos_condicao, $valores, $tabela);
    return $resultado;
  }
  public function query($sql) {
    return parent::query($sql);
  }
  
  public function insert($campos, $valores_campos, $tabela = 'flores') {
    $id_flor = parent::insert($campos, $valores_campos, $tabela); 
    return $id_flor;
  }
  
  public function getFlores(){
    $flores = $this->all('flores');
    return $flores; 

  }

  public function insertIntoFloresMeses( $flor_id,$meses ){
    if(count($meses) > 0) {
      foreach($meses as $mes){
        parent::insert(
          ['flor_id','mes_id'],
          [$flor_id, $mes],
          'flores_meses'
        );
      }
    }
    else{
      parent::insert(
          ['flor_id','mes_id'],
          [$flor_id, 0],
          'flores_meses'
        );
    }
  }
  public function insertIntoFloresAbelhas( $flor_id,$abelhas ){
    foreach($abelhas as $abelha){
      parent::insert(
        ['flor_id','abelha_id'],
        [$flor_id, intval($abelha)],
        'flores_abelhas'
      );
    }
  }
}