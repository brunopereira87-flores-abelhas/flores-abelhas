<?php

class Mes extends Model{
  public function where($campos_array, $campos_condicao, $valores, $tabela = "meses") {
    $resultado = parent::where($campos_array, $campos_condicao, $valores, $tabela);
    return $resultado;
  }
  public function query($sql) {
    return parent::query($sql);
  }
  
  public function insert($campo, $valor_campo, $tabela = 'meses') {
    $id_comentario = parent::insert($campo, $valor_campo, $tabela); 
    
    return $id_comentario;
  }
  
  public function getMeses(){
    $meses = $this->all('meses');
    return $meses; 

  }
  
}